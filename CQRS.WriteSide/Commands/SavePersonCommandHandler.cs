﻿using System.Threading.Tasks;
using Akka.Actor;
using CQRS.WriteSide.Database;
using CQRS.WriteSide.Database.Model;

namespace CQRS.WriteSide.Commands
{
    class SavePersonCommandHandler : ReceiveActor
    {
        public SavePersonCommandHandler()
        {
            ReceiveAsync<SavePerson>(Handle);
        }

        private async Task Handle(SavePerson savePerson)
        {
            var record = new PersonRecord
            {
                FirstName = savePerson.FirstName,
                LastName = savePerson.LastName
            };

            using (var context = new MySqlDbContext())
            {
                await context.People.AddAsync(record);
                await context.SaveChangesAsync();
            }
            
            Sender.Tell(new IdCommandResult(record.Id), Self);
        }
    }
}